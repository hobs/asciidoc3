// Test for lang-hu.conf language file.
:lang: hu

ASCIIDOC3(1)
===========
:doctype: manpage

NAME
----
asciidoc3 - converts an AsciiDoc3 text file to HTML or DocBook

ÁTTEKINTÉS
----------
*asciidoc3* ['OPTIONS'] 'FILE'

DESCRIPTION
-----------
The asciidoc3(1) command translates the AsciiDoc3 text file 'FILE' to
DocBook or HTML.  If 'FILE' is '-' then the standard input is used.

...
