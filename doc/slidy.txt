Slidy backend for AsciiDoc3
===========================
:author:    Stuart Rackham
:backend:   slidy
:max-width: 45em
:data-uri:
:icons:

Thx to Stuart Rackham, some additioinal information regarding 
AsciiDoc3 added by Berthold Gehrke.

Use the following command to create this document (`slidy.html`):

  asciidoc3 slidy.txt

Press the 'Space' key or the 'Left Arrow' key to continue the
slideshow.


Overview
--------
AsciiDoc3 has a 'slidy' backend for generating self contained 'Slidy'
HTML slideshows.

- An overview of 'Slidy' can be found https://www.w3.org/Talks/Tools/Slidy2/[here].

- AsciiDoc3 ships with the Slidy JavaScript a customised Slidy CSS
  file.
- Use the 'asciidoc3' `--backend slidy` command-line option to generate
  browser viewable HTML slideshow. Example:

  asciidoc3 --backend slidy slidy-example.txt

- By default, 'asciidoc3' creates a self contained distributable HTML
  document which includes all necessary CSS and JavaScripts.  Use the
  'asciidoc3' `--attribute linkcss` command-line option if you prefer
  to link (rather than embed) external CSS and JavaScript files.


How Slidy uses AsciiDoc3 markup
------------------------------
- The document header is displayed as the first page of the slideshow.
- If the document has a 'preamble' (the content between the header and
  the first section) it will be displayed on the second page.
- Each top level section is displayed as a separate slide.
- If the AsciiDoc3 'incremental' attribute is defined then all lists
  and OpenBlock elements are treated as 'incremental' i.e. Slidy
  exposes their child elements one at a time as you progress through
  the slideshow.
- You can explicitly set an element as incremental by setting its
  'role' attribute to 'incremental'. Example:

  [role="incremental"]
  Sagittis in vestibulum. Habitasse ante nulla enim bibendum nulla.
  Odio sed pede litora.


Slidy Specific Attributes
-------------------------
incremental::
If this attribute is defined then all lists and OpenBlock elements are
classed as incremental i.e. Slidy will expose them one at a time as
you progress through the slideshow.  The default behavior is for the
show to progress a full slide at at time.  Elements that have been
explicitly classed as 'incremental' with the 'role' attribute are
always displayed incrementally.

duration::
Set this to the estimated number of minutes that your presentation
will take and Slidy will display a countdown timer at the bottom right
of the slideshow. For example, this 'asciidoc' command-line option
`--attribute duration=5` sets the duration to five minutes.

copyright::
If you set the 'copyright' attribute it will be displayed in the Slidy
menu bar at the bottom of the slide. If the 'copyright' attribute is
not define then the author name, if it is defined, will be displayed.

outline::
Setting the 'role' attribute to 'outline' in a bulleted or numbered
list allows nested lists to be expanded and collapsed using the mouse.
Nested lists are expanded when an enclosing list item (the ones with
blue bullet points or numbers) is clicked.


Stylesheets
-----------
- 'slidy' loads the 'asciidoc.css' stylesheet followed by 'slidy.css':
  * `asciidoc3.css` contains content presentational styles.
  * The customized `slidy.css` stylesheet contains Slidy specific
    styles (table of contents, menu and transition related).
- You can use AsciiDoc3 CSS themes. For example, the 'asciidoc3'
  `--attribute theme=volnitsky` command-line option will substitute
  the `asciidoc3.css` stylesheet for the distributed Volnitsky
  `volnitsky.css` stylesheet.
- An additional stylesheet can be specified by setting the
  'stylesheet' attribute. For example, the `--attribute
  stylesheet=myslidy.css` will load `myslidy.css` after the standard
  CSS files.


Tips
----
- The Slidy 'help?' and 'contents?' links at the bottom left are your
  friends.
- Use the AsciiDoc3 'data-uri' attribute to embed images in your
  slideshow -- add this attribute entry to your document's header:

  :data-uri:

- Use the AsciiDoc3 'max-width' attribute to set the document display
  width. For example, add this attribute entry to your document's
  header:

  :max-width: 45em
